(function () {
  'use strict';
  var ip = '192.168.2.80';
  var porta = 102;
  var db = 1005;
  var pingTO = 1000;
  var sendTO = 1000;
  var recvTO = 1000;

  var bits = [{
      "nome": "deliberaHabilitada",
      "offset": 1608
    },
    {
      "nome": "presencaPeca",
      "offset": 1609
    },
    {
      "nome": "prontoParaIniciar",
      "offset": 1610
    },
    {
      "nome": "aguardandoDelibera",
      "offset": 1611
    },
    {
      "nome": "deliberaSucesso",
      "offset": 1612
    },
    {
      "nome": "lifeBit",
      "offset": 1200
    },
    {
      "nome": "deliberaAtiva",
      "offset": 1208
    },
    {
      "nome": "cicloEmCurso",
      "offset": 1209
    },
    {
      "nome": "fimCicloOk",
      "offset": 1210
    },
    {
      "nome": "anomaliaDelibera",
      "offset": 1215
    }
  ];

  var _db;

  var snap7 = require('node-snap7');

  var s7client = new snap7.S7Client();

  var configuraConexao = function (ip, port, db, pingTO, sendTO, recvTO) {
    if (s7client) {
      // configurações da conexão remota com PLC
      ipPLC = ip;
      _db = db;
      s7client.SetParam(s7client.RemotePort, port);
      s7client.SetParam(s7client.PingTimeout, pingTO);
      s7client.SetParam(s7client.SendTimeout, sendTO);
      s7client.SetParam(s7client.RecvTimeout, recvTO);
    } else {
      throw new Error('Erro com objeto s7client.');
    }
  };

  var conectaPlc = function (callback) {
    s7client.ConnectTo(ipPLC, 0, 0, function (err) {
      if (err) callback(s7client.ErrorText(err), false);
      else callback(null, true);
    });
  };

  var ativaBit = function (offset, callback) {
    s7client.WriteArea(s7client.S7AreaDB, _db, offset, 1, s7client.S7WLBit, new Buffer([0x01]), function (err) {
      if (err) callback(s7client.ErrorText(err));
      else callback();
    });
  };

  var desativaBit = function (offset, callback) {
    s7client.WriteArea(s7client.S7AreaDB, _db, offset, 1, s7client.S7WLBit, new Buffer([0x00]), function (err) {
      if (err) callback(s7client.ErrorText(err));
      else callback();
    });
  };

  var getBytes = function (offset, size, callback) {
    s7client.ReadArea(s7client.S7AreaDB, _db, offset, 2, s7client.S7WLByte, function (error, result) {
      if (callback) {
        if (error) callback(error, null);
        else callback(null, result);
      }
    });
  };

  var getBis = function (offset, size, callback) {
    s7client.ReadArea(s7client.S7AreaDB, _db, offset, 2, s7client.S7WLByte, function (error, result) {
      if (callback) {
        if (error) callback(error, null);
        else callback(null, result);
      }
    });
  };

  module.exports = {
    configuraConexao: configuraConexao,
    conectaPlc: conectaPlc,
    ativaBit: ativaBit,
    desativaBit: desativaBit,
    getBytes: getBytes
  };
})();