(function () {

  const express = require('express');
  const path = require('path');
  const router = express.Router();

  // middleware específico para este router
  router.use(function timeLog(req, res, next) {
    console.log('Time: ', Date.now());
    next();
  });

  // define página inicial 
  router.get('/', function (req, res) {
    let file = path.resolve(__dirname, '../frontend/index.html'); 
    res.sendFile(file);
  });

  // endpoint que dispara evento
  router.post('/', (req, res) => {
    console.log(JSON.stringify(req.body));
    let to = req.body.To;
    let fromNumber = req.body.From;
    let callStatus = req.body.CallStatus;
    let callSid = req.body.CallSid;
  
    io.emit('test event', { to, fromNumber, callStatus, callSid });
  
    console.log(to, from, callStatus, callSid);
    res.send('Event received');
  });

  module.exports = router;

})()