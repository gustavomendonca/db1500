(function () {

  const express = require('express');
  const bodyParser = require('body-parser');
  // var plc = require('plc');

  const app = express();
  const _default = require('./routes/default');

  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));

  app.set('port', (process.env.PORT || 3600));

  app.use('/api', _default);

  app.use(express.static('./frontend')); // adiciona middleware
  
  app.use(function(request, response, next){
    // faço algo... ou não, desencana..
    next(); // chama próximo middleware
  });

  // const logger = require('./logger');
  // app.use(logger); // se não vamos fazer nada, melhor deixar registrado isso!

  app.use(function(request, response, next){
    // hey, eu também não faço nada... melhor terminar por aqui e responder pq usuário espera
    response.send('...não fizemos nada!');
  })

  app.get('/bits', function (req, res) {

    // use mongoose to get all todos in the database
    // Todo.find(function (err, todos) {

    //   // if there is an error retrieving, send the error. nothing after res.send(err) will execute
    //   if (err)
    //     res.send(err)

    //   res.json(todos); // return all todos in JSON format
    // });
  });

  const server = app.listen(app.get('port'), function () {
    console.log('Node app is running on port', app.get('port'));
  });

  const io = require('socket.io')(server);

  io.on('connection', (socket) => {
    console.log('a user connected');

    socket.on('disconnect', () => {
      console.log('user disconnected');
    });
  });

})();