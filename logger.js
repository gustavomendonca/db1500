module.exports = function(request, response, next){
    var start = +new Date(); // sinal '+' transforma objeto data em milisegundos !!
    var stream = process.stdout;
    var url = request.url;
    var method = request.method;

    // obj response é um event emitter
    response.on('finish', function(){
      var duration = +new Date() - start;
      var msg = method + ' ' + url + ' demorou ' + duration + 'ms\n';
      stream.write(msg); // envia mensagem para stdout 
    });

    next();
}